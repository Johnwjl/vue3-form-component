/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution')

module.exports = {
  root: true,
  'extends': [
    'plugin:vue/vue3-essential',
    'eslint:recommended',
    '@vue/eslint-config-typescript',
    '@vue/eslint-config-prettier/skip-formatting'
  ],
  parserOptions: {
    ecmaVersion: 'latest'
  },
  "no-spaced-func": 2, //函数调用时 函数名与()之间不能有空格
  "no-trailing-spaces": 1,//一行结束后面不要有空格
  "key-spacing": [1, { "beforeColon": false, "afterColon": true }],//对象字面量中冒号的前后空格
}
